package com.android.lib.messenger

import android.os.Bundle
import com.android.lib.messenger.utils.Const
import java.util.concurrent.ConcurrentHashMap

object MessageProcessor {

    private var subscribers: ConcurrentHashMap<String, MessageCallback> = ConcurrentHashMap<String, MessageCallback>()

    fun subscribe(key: String, callback: MessageCallback) {
        subscribers[key] = callback
    }

    fun unSubscribe(key: String) {
        subscribers.remove(key)
    }

    fun onMessageReceive(bundle: Bundle) {
        val key = bundle.getString(Const.KEY_MESSAGE)
        if (null != key) {
            subscribers[key]?.onMessageCallBack(bundle)
        }
    }

}