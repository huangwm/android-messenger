package com.android.lib.messenger.utils

import android.app.ActivityManager
import android.content.Context
import android.os.Process

object ProcessUtils {

    /**
     * 包名判断是否为主进程
     */
    fun isMainProcess(context: Context): Boolean {
        return context.packageName == getProcessName(context)
    }

    /**
     * 获取进程名称
     *
     * @param context 上下文
     * @return 进程名
     */
    fun getProcessName(context: Context): String? {
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningApps = am.runningAppProcesses ?: return null
        for (proInfo in runningApps) {
            if (proInfo.pid == Process.myPid()) {
                if (null != proInfo.processName) {
                    return proInfo.processName
                }
            }
        }
        return null
    }

}