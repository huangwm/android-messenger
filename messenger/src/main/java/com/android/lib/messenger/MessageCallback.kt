package com.android.lib.messenger

import android.os.Bundle

interface MessageCallback {

    /**
     * 接收消息回调
     *
     * @param data 消息内容
     */
    fun onMessageCallBack(data: Bundle?)

}