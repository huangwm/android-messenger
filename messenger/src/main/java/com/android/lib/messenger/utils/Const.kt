package com.android.lib.messenger.utils

object Const {

    //订阅
    var SUBSCRIBE : Int
        get() = 1
        set(value) { SUBSCRIBE = value }

    //取消订阅
    var UNSUBSCRIBE: Int
        get() = -1
        set(value) { UNSUBSCRIBE = value }

    //发送消息给指定目标
    var SEND_MSG_TO_TARGET: Int
        get() = 2
        set(value) { SEND_MSG_TO_TARGET = value }

    //传递key的关键字
    var KEY_MESSAGE: String
        get() = "messenger_key"
        set(value) { KEY_MESSAGE = value }


}