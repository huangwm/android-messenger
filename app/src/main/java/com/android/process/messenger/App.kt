package com.android.process.messenger

import android.app.Application
import com.android.lib.messenger.AndroidMessenger

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        AndroidMessenger.init(this)
    }


}