package com.android.process.messenger

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.lib.messenger.AndroidMessenger
import kotlinx.android.synthetic.main.activity_test_2.*

class TestActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_2)
        button1.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("test", "TestActivity2 key:activity2 发送的消息")
            AndroidMessenger.post("Activity2ToActivity1", bundle)
        }
    }

}